import yfinance as yf
from yahoo_fin import stock_info
from datetime import date, timedelta

def get_quote_data(symbol):
    """Get quote data a ticker symbol

    Args:
        symbol (str): ticker symbol

    Returns:
        dict: quote data
    """
    try:
        stock = yf.Ticker(symbol)
        return stock.info
    except Exception as e:
        raise Exception(f"get_quote_data() failed: {e}")

def get_data(symbol, days, interval="1m"):
    """Get quote data a ticker symbol

    Args:
        symbol (str): ticker symbol

    Returns:
        dataframe
    """
    if interval == "1m":
        days = min(6, days)

    try:
        today = date.today()
        df = stock_info.get_data(
            symbol,
            start_date=today - timedelta(days=days),
            end_date=today,
            interval=interval,
        )
        return df
    except Exception as e:
        raise Exception(f"get_graph_data() failed: {e}")


def get_error_messages(e):
    """Get messages to display from an Exception

    Args:
        e (Exception): Exception

    Returns:
        dict: top, middle en bottom messages to display
    """
    return {
        "top": "",
        "middle": "ERROR",
        "bottom": str(e)[: min(len(str(e)), 25)] + "...",
    }


def get_simple_messages(symbol, quote_data):
    """Get messages to display depending on market status and other things

    Args:
        quote_data (dict): quote data returned by yahoo_fin

    Returns:
        dict: top, middle en bottom messages to display
    """

    price = quote_data.get('regularMarketPrice', quote_data.get('currentPrice'))
    price = round(price, 2) if price is not None else price

    messages = {
        "top": f"${symbol}",
        "middle": f"{price:,}",
        "bottom": "",
    }

    market_state = quote_data.get("marketState", "UNKNOWN")
    messages["bottom"] = f"Market: {market_state}"

    if market_state == "OPEN":
        pass
    elif market_state == "PRE":
        messages["bottom"] += f" > {get_rounded(quote_data, 'preMarketPrice')}"
    elif market_state in ["POST", "CLOSED", "PREPRE"]:
        messages["bottom"] += f" > {get_rounded(quote_data, 'postMarketPrice')}"

    return messages
